﻿using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Xml.XPath;

namespace xml_test
{
    public class XmlDocMgr : INotifyPropertyChanged
    {
        private XPathNavigator _nav;

        public XmlDocMgr(string fileName)
        {
            InitXmlDoc(fileName);
        }
        //---------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="propName"></param>
        protected void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null && !string.IsNullOrEmpty(propName))
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propName));
        }
        //---------------------------------------------------------------------

        /// <summary>
        /// Данные xml документа.
        /// </summary>
        public string Content { get; set; }

        public XPathDocument XpDoc { get; private set; }

        private string _XPathValue;
        /// <summary>
        /// XPath выражение.
        /// </summary>
        public string XPathValue
        {
            get
            {
                return _XPathValue;
            }
            set
            {
                _XPathValue = value;
                OnPropertyChanged(nameof(this.XPathValue));
            }
        }

        private object _XPathResult;
        /// <summary>
        /// Результат применения XPath.
        /// </summary>
        public object XPathResult
        {
            get
            {
                return _XPathResult;
            }
            set
            {
                _XPathResult = value;
                OnPropertyChanged(nameof(this.XPathResult));
            }
        }
        //---------------------------------------------------------------------

        /// <summary>
        /// Инициализация.
        /// </summary>
        /// <param name="fileName"></param>
        private void InitXmlDoc(string fileName)
        {
            if (File.Exists(fileName))
            {
                Content = File.ReadAllText(fileName);
                XpDoc = new XPathDocument(fileName);
                _nav = XpDoc?.CreateNavigator();
            }
        }

        /// <summary>
        /// Выборка данных по выражению и формирование результата.
        /// </summary>
        public void ApplyXPath()
        {
            try
            {
                var evalResult = _nav?.Select(_XPathValue);
                if (evalResult != null)
                {
                    int i = 1;
                    var tempResult = string.Empty;
                    while (evalResult.MoveNext())
                    {
                        tempResult = $"{tempResult} --- Результат №{i} ---\r\n{evalResult.Current.OuterXml}\r\n\r\n";
                        i++;
                    }
                    XPathResult = tempResult;
                }
            }
            catch (XPathException ex)
            {
                MessageBox.Show($"Неверный формат выражения\r\n{ex.Message}", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                XPathResult = string.Empty;
            }
        }
    }
}