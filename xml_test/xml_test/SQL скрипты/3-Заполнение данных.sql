use st_db

delete from BankAccounts
delete from Clients

set identity_insert Clients on
insert into Clients (Id, Caption, Address) values (1, '��� "�������� ����"', '��, �. �����, ��. �������, 9')
insert into Clients (Id, Caption, Address) values (2, '��� "����� � �����"', '��, �. �����, ��. ����������, 34')
insert into Clients (Id, Caption, Address) values (3, '��� "���� ��������"', '��, �. ������, ��. ����� ������, 12')
insert into Clients (Id, Caption, Address) values (4, '��� "��� ����"',      '��, �. ������, ��. ������, 87')
set identity_insert Clients off

set identity_insert BankAccounts on
insert into BankAccounts (Id, Number, CurrencyCode, OpeningDate, ClosingDate, Balance, ClientId) values (1, 1001, 'BYN', '2012-01-10', '2020-02-25', 110.10, 1)
insert into BankAccounts (Id, Number, CurrencyCode, OpeningDate, ClosingDate, Balance, ClientId) values (2, 1002, 'BYN', '2012-02-11', '2020-02-26', 120.20, 1)
insert into BankAccounts (Id, Number, CurrencyCode, OpeningDate, ClosingDate, Balance, ClientId) values (3, 1003, 'BYN', '2012-03-12', '2020-03-27', 130.30, 1)
insert into BankAccounts (Id, Number, CurrencyCode, OpeningDate, ClosingDate, Balance, ClientId) values (4, 1004, 'BYN', '2012-04-13', '2020-04-28', 140.40, 1)

insert into BankAccounts (Id, Number, CurrencyCode, OpeningDate, Balance, ClientId) values (5, 2001, 'USD', '2014-01-11', 210.10, 2)
insert into BankAccounts (Id, Number, CurrencyCode, OpeningDate, Balance, ClientId) values (6, 2002, 'USD', '2014-02-13', 220.20, 2)
insert into BankAccounts (Id, Number, CurrencyCode, OpeningDate, Balance, ClientId) values (7, 2003, 'USD', '2014-03-14', 230.30, 2)
insert into BankAccounts (Id, Number, CurrencyCode, OpeningDate, Balance, ClientId) values (8, 2004, 'USD', '2014-04-15', 240.40, 2)

insert into BankAccounts (Id, Number, CurrencyCode, OpeningDate, ClosingDate, Balance, ClientId) values (9,  3001, 'EUR', '2012-01-12', '2020-09-18', 310.10, 3)
insert into BankAccounts (Id, Number, CurrencyCode, OpeningDate, ClosingDate, Balance, ClientId) values (10, 3002, 'EUR', '2012-02-13', '2020-12-25', 320.20, 3)
insert into BankAccounts (Id, Number, CurrencyCode, OpeningDate, ClientId) values (11,  3003, 'EUR', '2016-01-23', 3)
insert into BankAccounts (Id, Number, CurrencyCode, OpeningDate, ClientId) values (12,  3004, 'EUR', '2016-03-24', 3)
set identity_insert BankAccounts off