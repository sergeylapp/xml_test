-- �������� �� ��������� ��������, ����� �������
create database st_db
collate Cyrillic_General_CI_AS
go

use st_db

--drop table BankAccounts
--drop table Clients
--drop table LogEvents

create table Clients
(
    Id int identity(1,1) primary key not null,
    Caption nvarchar(50) not null,
    Address nvarchar(200)
); 

create table BankAccounts
(
    Id int identity(1,1) primary key not null,
    Number nvarchar(10) not null,
    CurrencyCode nvarchar(3) not null,
    OpeningDate datetime not null,
    ClosingDate datetime,
    Balance decimal(15,2),
    ClientId int foreign key references Clients(Id)
); 

create table LogEvents
(
    Id int identity(1,1) primary key not null,
    EventDate datetime default CURRENT_TIMESTAMP,
    Description nvarchar(200)
);