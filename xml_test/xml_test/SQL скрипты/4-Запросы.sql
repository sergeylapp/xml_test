use st_db

--4. ���������� �������� ������,
--������� ���������� ������ ��������, � ������� ���� �����, ���������� ���� ������ � ����� ������ ���� �������� ����� ���� ������.

select Clients.Caption, count(*) as BankAccounts, min(OpeningDate) as FirstOpeningDate from BankAccounts
join Clients on BankAccounts.ClientId = Clients.Id
where OpeningDate is not null
group by Clients.Caption

--5. ���������� �������� ������,
--������� ���������� ������ ��������, � ������� ��� ����� ������� � ����� ������� ���� �������� ����� ������ ������� �������.

select clnts.Id as ClientId, clnts.Caption ClientCaption, max(ClosingDate) as LastClosingDate from BankAccounts
join Clients as clnts on BankAccounts.ClientId = clnts.Id
where BankAccounts.ClosingDate is not null
group by clnts.Id, clnts.Caption
having count(*) = (select count(*) from BankAccounts where ClientId = clnts.Id)

--6. ���������� �������� ������,
--������� ���������� ������ ������ ��������, � ������� � ������������ ���� ���.

select * from BankAccounts
join Clients on BankAccounts.ClientId = Clients.Id
where Clients.Caption like '%���%'

--7. ���������� �������� ������,
--������� ��� �������� � ��������������, ������������� � �������, ����������� ������� ������� ����� ������ 0, ���� �� ��� ����������.

declare @qr nvarchar(20) = '%����%'
update BankAccounts set Balance=0
where Balance is null and ClientId in (select Id from Clients where Caption like @qr)

--8. ���������� �������� ������,
--������� ���������� ������ ���������� ������ ������ � ������������� �������, �������� ����������� ����.

select BankAccounts.*, Clients.Caption as Client from BankAccounts
join Clients on ClientId = Clients.Id
where BankAccounts.ClosingDate is null