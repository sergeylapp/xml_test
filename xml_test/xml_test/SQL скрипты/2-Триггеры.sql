create trigger Clients_INSERT
on Clients
after insert
as
insert into LogEvents (EventDate, Description) select CURRENT_TIMESTAMP, '�������� ������, Id: ' + STR(Id)
from Inserted
go

create trigger BankAccounts_INSERT
on BankAccounts
after insert
as
insert into LogEvents (EventDate, Description) select CURRENT_TIMESTAMP, '�������� ����, Id: ' + STR(Id)
from Inserted
go

create trigger Clients_UPDATE
on Clients
after update
as
insert into LogEvents (EventDate, Description) select CURRENT_TIMESTAMP, '������� ������, Id: ' + STR(Id)
from Inserted
go

create trigger BankAccounts_UPDATE
on BankAccounts
after update
as insert into LogEvents (EventDate, Description) select CURRENT_TIMESTAMP, '������� ����, Id: ' + STR(Id)
from Inserted
go

create trigger Clients_DELETE
on Clients
after delete
as insert into LogEvents (EventDate, Description) select CURRENT_TIMESTAMP, '����� ������, Id: ' + STR(Id)
from Deleted
go

create trigger BankAccounts_DELETE
on BankAccounts
after delete
as insert into LogEvents (EventDate, Description) select CURRENT_TIMESTAMP, '����� ����, Id: ' + STR(Id)
from Deleted
go