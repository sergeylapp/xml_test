﻿using Microsoft.Win32;
using System.Windows;

namespace xml_test
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private XmlDocMgr xmlDoc { get; set; }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void SelectXmlDoc()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Документ XML|*.xml";
            openFileDialog.Multiselect = false;
            if (openFileDialog.ShowDialog() == true)
            {
                xmlDocTextBox.Text = openFileDialog.FileName;
                xmlDoc = new XmlDocMgr(openFileDialog.FileName);

                xpathTextBox.DataContext = xmlDoc;
                originalXmlTextBox.DataContext = xmlDoc;
                resultXPathTextBox.DataContext = xmlDoc;

                xpathTextBox.IsEnabled = true;
                applyXpathButton.IsEnabled = true;
            }
        }

        private void seleXmlDocButton_Click(object sender, RoutedEventArgs e)
        {
            SelectXmlDoc();
        }

        private void applyXpathButton_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(xmlDoc.XPathValue))
            {
                MessageBox.Show("Требуется выражение XPath", "Предупреждение", MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }

            xmlDoc.ApplyXPath();
        }
    }
}